import Firebase from "firebase";

const firebaseApp = Firebase.initializeApp({
  apiKey: "AIzaSyBlfhT8q3QnniPdIUFzf_xpC5Gf9cR2YKw",
  authDomain: "vue-drujemetr.firebaseapp.com",
  databaseURL: "https://vue-drujemetr.firebaseio.com",
  projectId: "vue-drujemetr",
  storageBucket: "vue-drujemetr.appspot.com",
  messagingSenderId: "953066042114"
});

// Export the database for components to use.
// If you want to get fancy, use mixins or provide / inject to avoid redundant imports.
export const db = firebaseApp.database();
export const auth = firebaseApp.auth();
